# PyFF: Python Fourier Filter
Toy software to play with 2D Fourier and filtering it

Real-time filtering of the webcam image

PC version. Android version is [here](android/).

## Installation
- Requires Python3
- Requires Python libraries numpy and openCV (cv2)
- Requires a webcam

## Usage
- h/H: display this help
- i/I: invert filter
- r/R: reset filter
- s/S: toggle symmetry in mask drawing
- w/W: change apodisation
- 0-6: predefined filters
- q/Q: quit

Draw the filter by clicking on the spectrum.
- 0-6: different pre-defined LP and HP filters

## Inspiration
FourierFilterCam: https://github.com/zfphil/FourierFilterCam

## Screenshots
![pyff screenshot raw](pyff_raw.jpg)

![pyff screenshot filter](pyff_filter.jpg)
