#!/usr/bin/python3
import cv2
import numpy as np
from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.button import Button
from kivy.uix.togglebutton import ToggleButton
from kivy.uix.camera import Camera
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.graphics.texture import Texture
from android.permissions import request_permissions, Permission


class Pyff(Camera):
    def __init__(self, resolution, btn_size, draw_size):
        super().__init__()
        self.resolution = resolution
        self.btn_size = btn_size
        self.draw_size = draw_size
        self.allow_stretch = True
        self.symmetry = False
        self.fourier_display = False
        self.apod_size = 0
        self.help_display = False
        self.update()
        self.help_screen = None

    def _camera_loaded(self, *args):
        if self._camera.texture is not None:
            self.texture = self._camera.texture
            self.texture = Texture.create(size=np.flip(self.resolution),
                                          colorfmt='luminance')
            self.texture_size = list(self.texture.size)
            self.mask = np.ones(self.resolution)
            self.apodization(None)

    def on_tex(self, *args):
        self.update()

    def update(self):
        if self._camera._buffer:
            w, h = self.resolution
            frame = np.frombuffer(self._camera._buffer.tostring(), 'uint8')
            frame = frame.reshape((h + h // 2, w))
            gray = cv2.cvtColor(frame, cv2.COLOR_YUV420p2GRAY)
            gray = np.rot90(gray, 3)
            gray = cv2.resize(gray, (self.resolution[1], self.resolution[0]))
            fourier = np.fft.fft2(gray*self.window)
            if self.fourier_display:
                img = np.log(np.abs(np.fft.fftshift(fourier)))
                img = img/img.max()
                img = img*self.mask
                img = np.uint8(img*255)
                img = np.flipud(img)
            else:
                img = fourier*np.fft.ifftshift(self.mask)
                img = np.abs(np.fft.fft2(img))
                img = np.uint8(img/img.max()*255)
                img = np.fliplr(img)
            self.texture.blit_buffer(img.tobytes(),  bufferfmt="ubyte",
                                     colorfmt="luminance")
            self.canvas.ask_update()

    def on_touch_down(self, touch):
        if self.collide_point(touch.x, touch.y):
            self.oldx, self.oldy = self.touch2maskxy(touch)

    def on_touch_move(self, touch):
        if self.collide_point(touch.x, touch.y) and self.fourier_display:
            x, y = self.touch2maskxy(touch)
            if x and y:
                cv2.line(self.mask, (self.oldx, self.oldy), (x, y), 0,
                         self.draw_size)
                if self.symmetry:
                    sym_oldx = self.resolution[1] - self.oldx
                    sym_oldy = self.resolution[0] - self.oldy
                    sym_x = self.resolution[1] - x
                    sym_y = self.resolution[0] - y
                    cv2.line(self.mask, (sym_oldx, sym_oldy), (sym_x, sym_y),
                             0, self.draw_size)

                self.oldx, self.oldy = x, y

        self.update()

    def touch2maskxy(self, touch):
        if self.width/self.height >= self.resolution[1]/self.resolution[0]:
            # compare the screen proportion to the image proportion
            # image is stretched limited by its height
            ratio = self.resolution[0] / self.height
            y = int((self.height - (touch.y-self.btn_size)) * ratio)
            dx = touch.x - self.center_x
            if abs(dx*ratio) < self.resolution[1]/2:
                x = int(dx*ratio + self.resolution[1]/2)
            else:  # outside of picture
                x = None
        else:
            ratio = self.resolution[1] / self.width
            x = int(touch.x * ratio)
            dy = (self.height-(touch.y-self.btn_size)) - self.center_y
            if abs((dy + self.btn_size)*ratio) < self.resolution[0]/2:
                y = int((dy+self.btn_size)*ratio + self.resolution[0]/2)
            else:  # outside of picture
                y = None
        return x, y

    def clear(self, _):
        self.mask = np.ones(self.resolution)
        self.update()

    def sym(self, _):
        print("symétrie")
        self.symmetry = not(self.symmetry)

    def invert(self, _):
        self.mask = 1 - self.mask
        self.update()

    def switch(self, _):
        self.fourier_display = not(self.fourier_display)
        self.update()

    def apodization(self, _):
        if self.apod_size > 0:
            halfhann = 1/2+1/2*np.cos(np.linspace(0, np.pi, self.apod_size))
            row = np.ones((self.resolution[0], 1))
            row[:self.apod_size, 0] = halfhann[::-1]
            row[-self.apod_size:, 0] = halfhann
            col = np.ones((1, self.resolution[1]))
            col[0, :self.apod_size] = halfhann[::-1]
            col[0, -self.apod_size:] = halfhann
            self.window = (np.dot(row, np.ones(col.shape))
                           * np.dot(np.ones(row.shape), col))
        else:
            self.window = np.ones(self.resolution)
        self.apod_size = (self.apod_size+10) % 40
        self.update()

    def help(self, _):
        if not self.help_screen:
            self.help_screen = Label(pos=(self.width/2, self.height/2))
            self.help_screen.text = ("     PyFF: Python Fourier Filter\n" +
                                     "\n" +
                                     "Four: switch to Fourier spectrum\n" +
                                     "Inv: invert filter\n" +
                                     "Sym: toggle symmetry in mask drawing\n" +
                                     "Win: change apodisation\n" +
                                     "Clear: reset filter\n" +
                                     "Help: display this help")
        self.help_display = not(self.help_display)
        if self.help_display:
            self.add_widget(self.help_screen)
        else:
            self.remove_widget(self.help_screen)


class FourierFilter(App):
    def build(self):
        btn_size = 300
        resolution = (640, 480)
        draw_size = 20
        screen = BoxLayout(orientation="vertical")
        pyff = Pyff(resolution=resolution, btn_size=btn_size,
                    draw_size=draw_size)

        buttons = BoxLayout(orientation="horizontal", size_hint=(1, None),
                            height=btn_size)
        btn1 = Button(text='Four', on_press=pyff.switch)
        btn2 = Button(text='Inv', on_press=pyff.invert)
        btn3 = ToggleButton(text='Sym', on_press=pyff.sym)
        btn4 = Button(text='Win', on_press=pyff.apodization)
        btn5 = Button(text='Clear', on_press=pyff.clear)
        btn6 = ToggleButton(text='Help', on_press=pyff.help)
        buttons.add_widget(btn1)
        buttons.add_widget(btn2)
        buttons.add_widget(btn3)
        buttons.add_widget(btn4)
        buttons.add_widget(btn5)
        buttons.add_widget(btn6)

        screen.add_widget(pyff)
        screen.add_widget(buttons)

        return screen


if __name__ == '__main__':
    request_permissions([Permission.CAMERA])
    FourierFilter().run()
