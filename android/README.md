# PyFF: Python Fourier Filter
Android version, in debug mode. Uses [kivy](https://kivy.org) as User Interface.

## Installation
Download and install the [apk](https://www.creatis.insa-lyon.fr/nextcloud/index.php/s/2GY8sLgYJ9P2jcs).

## Packaging
The proposed apk has been packaged using [Buildozer](https://buildozer.readthedocs.io/en/latest/).
```
buildozer -v android debug
```

## Known bug
When running for the first time Pyff, you are asked for the permission to use the camera, and then Pyff crashes.
No more crashes (hopefully) after.
